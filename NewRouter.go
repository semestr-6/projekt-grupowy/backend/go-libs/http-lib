package httplib

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/rs/cors"

	"github.com/gorilla/mux"
)

type Route struct {
	HttpMethod  string
	Route       string
	HandlerFunc http.HandlerFunc
	Scope       []string
}

type Routes []Route

func NewRouter(routes Routes) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = customPrometheus(handler)
		handler = cors.AllowAll().Handler(handler)
		router.Methods(route.HttpMethod).Path(route.Route).Handler(handler)
	}

	return router
}

func NewRouterWithAutorization(routes Routes) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	authURL := os.Getenv("AUTH_URL")
	for _, route := range routes {
		var handler http.Handler
		if route.HttpMethod != "OPTIONS" {
			handler = autorize(route.HandlerFunc, authURL)
		} else {
			handler = route.HandlerFunc
		}
		handler = customPrometheus(handler)
		handler = cors.AllowAll().Handler(handler)
		router.Methods(route.HttpMethod).Path(route.Route).Handler(handler)
	}

	return router
}

func autorize(function http.HandlerFunc, authURL string) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("Authorize")
		if token == "" {
			res.WriteHeader(http.StatusUnauthorized)
			return
		}

		fmt.Println(req.URL)
		endpoint, err := json.Marshal(req.URL.Path)

		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}

		status, err := SendAuthRequest(authURL, token, strings.NewReader(string(endpoint)))

		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}

		if status == 200 {
			function(res, req)
		} else {
			res.WriteHeader(status)
			return
		}
	}
}

func DeclareNewRouterWithAuthorizationAndStart(routes Routes) {
	router := NewRouterWithAutorization(routes)
	err := http.ListenAndServe(":80", router)

	log.Fatal(err)

}

func DeclareNewRouterAndStart(routes Routes) {
	router := NewRouter(routes)
	err := http.ListenAndServe(":80", router)

	log.Fatal(err)

}
