package httplib

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var histogram *prometheus.HistogramVec

func init() {
	log.Println("Prometheus init.")
	histogram = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "request_second",
		Help: "Response time",
	}, []string{"code"})
	prometheus.Register(histogram)
}

func customPrometheus(inner http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		code := http.StatusInternalServerError

		inner.ServeHTTP(w, r)

		code = http.StatusOK
		since := time.Since(start)
		histogram.
			WithLabelValues(fmt.Sprintf("%d", code)).
			Observe(since.Seconds())
		log.Println(since)
	})
}
