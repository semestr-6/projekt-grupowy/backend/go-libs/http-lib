package httplib

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func SendRequest(method string, url string, body io.Reader, timeout time.Duration) (responsebody []byte, err error) {
	client := http.Client{}

	if timeout == 0 {
		client.Timeout = time.Second * 2
	} else {
		client.Timeout = time.Second * timeout
	}

	req, reqErr := http.NewRequest(method, url, body)
	if reqErr != nil {
		log.Fatal(reqErr)
		err = reqErr
		return
	}

	res, getErr := client.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		err = getErr
		return
	}

	responsebody, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		err = readErr
		return
	}

	return
}

func SendAuthRequest(url, token string, body io.Reader) (statuscode int, err error) {
	client := http.Client{}
	client.Timeout = time.Second * 2

	req, reqErr := http.NewRequest("POST", url, body)
	if reqErr != nil {
		log.Fatal(reqErr)
		err = reqErr
		return
	}
	req.Header.Add("Authorize", token)
	req.Header.Add("accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	res, getErr := client.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		err = getErr
		return
	}
	statuscode = res.StatusCode

	return
}
