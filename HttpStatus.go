package httplib

import (
	"net/http"
)

func RespondStatus200(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Access-Control-Allow-Origin", "*")
	response.Header().Set("Access-Control-Allow-Headers", "Authorization,Content-Type")
	response.WriteHeader(http.StatusOK)
}

//func RespondStatusError
